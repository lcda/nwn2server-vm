
# Windows server 2012

## Guest tools
- Download & install https://www.spice-space.org/download/windows/spice-guest-tools/spice-guest-tools-latest.exe

## NFS setup

#### Method 1
- Right click on start menu, _Programs and Features_
- _Turn windows features on or off_
- Click next until Features screen
- Tick _Client for NFS_
- Tick _Remote Server Administration Tools > Role Administration Tools > File Services Tools > Services for Network File System Management Tools_
- Continue to install

#### Method 2
- From powershell, run `Install-WindowsFeature NFS-Client, RSAT-NFS-Admin`

#### Mount
- From the manager, go to _Tools_ / _Services for network file system_
    + Go to _Client for NFS_ properties
    + Tick all security flavors
- Open command prompt
    + `mount "\\10.0.2.2\home\crom\Documents\Neverwinter Nights 2" Z:`

Note: You can also make a symlink in nwn2 temp directory (set in nwnx.ini) to write logs to the NFS shared folder: `mklink /D LOGS.0 Z:\logs`

## NWN2Server setup
- Install `vcredist_2005_x86`
- Install `vcredist_2010_x86`
- Install [DirectX End-User Runtimes (June 2010)](https://www.microsoft.com/en-us/download/details.aspx?id=8109)
- Add windows feature: .NET framework 3.5
- Open port 5121 tcp and udp on windows firewall


## Starting nwn2server on boot
- Install [Autologon for windows](https://docs.microsoft.com/en-us/sysinternals/downloads/autologon)
- Setup autologon for administrator account
- Open the task manager
    + Add a task to mount and start the nwn2server, ie execute a bat with:
        ```
        mount "\\10.0.2.2\home\crom\Documents\Neverwinter Nights 2" Z:
        cd C:\Users\Administrator\Desktop\NWNX4
        NWNX4_GUI.exe
        ```

# Host configuration
## NFS server

- /etc/nfs.conf
    ```
    [nfsd]
    host=127.0.0.1
    ```
- /etc/exports
    ```
    "/home/crom/Documents/Neverwinter Nights 2"   127.0.0.1(insecure,rw,all_squash,fsid=root,crossmnt,no_subtree_check,anonuid=1000,anongid=1000)
    ```
- Enable and start NFS server service