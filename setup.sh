#!/bin/sh

set -e

if [ ! -f "$1" ]; then
	echo "Usage: $0 installdiskpath"
	exit 1
fi
INSTALLDISK="$1"

DISKIMAGE=lcdaserver.qcow2
if [ ! -f "$DISKIMAGE" ]; then
	echo "Disk image creation..."
	qemu-img create -f qcow2 "$DISKIMAGE" 15G
fi

if [ ! -f virtio-win.iso ]; then
	echo "Downloading virtio drivers..."
	wget https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/stable-virtio/virtio-win.iso
fi

echo "Will now start virtual machine with install disk '$INSTALLDISK'"
read

echo
echo "NOTES:"
echo " - During installation at the partition step Windows doesn't detect the VirtIO hard drive. You have to tell Windows to use the viostor driver from the driver image."
echo " - After installation Windows doesn't detect the VirtIO ethernet adapter. You have to tell Windows to use the netkvm driver from the driver image."
echo
echo

export DISKIMAGE
./launch.sh -boot d -drive file="$INSTALLDISK",media=cdrom -drive file=virtio-win.iso,media=cdrom
