#!/bin/sh

# TODO:
# add -runas crom

exec \
qemu-system-x86_64 \
	-name "LcdaServer" \
	\
	-enable-kvm \
	-cpu host \
	-smp cores=2,threads=1,sockets=1 \
	-m 2G \
	\
	-drive file="$DISKIMAGE",if=virtio \
	\
	-device virtio-net-pci,netdev=net0 \
	-netdev user,id=net0,hostname=lcdaserver,hostfwd=tcp:0.0.0.0:5121-:5121,hostfwd=udp:0.0.0.0:5121-:5121 \
	\
	-monitor stdio \
	\
	-vga qxl \
	-spice port=3389,disable-ticketing \
	-usbdevice tablet \
    -device virtio-serial \
    -chardev spicevmc,id=vdagent,name=vdagent \
    -device virtserialport,chardev=vdagent,name=com.redhat.spice.0 \
	$@
